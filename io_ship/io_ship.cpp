// io
#include <iostream>
#include <string>

typedef struct {
	int a;
	int b;
} str_t;

int main(void)
{
	char ch;

	str_t s1;
	str_t s2;

	std::cin >> s1.a >> ch >> s1.b;
	//std::cout << std::endl;

	std::cin >> s2.a >> ch >> s2.b;
	//std::cout << std::endl;

	std::cout << s1.a << ":" << s1.b << std::endl;
	std::cout << s2.a << ":" << s2.b << std::endl;
}