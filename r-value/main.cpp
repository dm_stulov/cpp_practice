// pointers and references: указатели и ссылка

#include <iostream>
#include <vector>

// R-value
// https://habr.com/ru/post/226229/
// https://tproger.ru/articles/move-semantics-and-rvalue/
// https://github.com/MicrosoftDocs/cpp-docs/blob/master/docs/cpp/move-constructors-and-move-assignment-operators-cpp.md
// https://github.com/gcc-mirror/gcc/blob/master/libstdc%2B%2B-v3/include/bits/move.h

/* Выводы от 2019-03-11

gcc move: https://github.com/gcc-mirror/gcc/blob/master/libstdc%2B%2B-v3/include/bits/move.h, строка 99. 

Сам move ничего не очищает - он просто указывает компилятору, что при создании или присваивании нужно взять rvalue, то есть использовать move конструктор или move присваивание. А эти сущности написаны так, что чистят исходный объект - скорее всего, предварительно копируя _ссылки_ на внутренние данные исходного объекта. Чистка исходных данных нужна для корректной деструкции (освобождения ресурсов). 


Вот простая проверка std::move:

std::vector s={'a', 'b'};
std::move(s);
std::cout << s.size() << std::endl; //  =2
std::vector t(std::move(s)); // будет вызван конструктор move вектора, и он почистит исходный
std::cout << s.size() << std::endl; //  =0

Для callback - указателя на функцию нужно чистить самому, если это нужно по логике работы.

*/

template <typename T>
void swap(std::vector<T> & a, std::vector<T> & b)
{
    std::cout << __LINE__ << ":" << a.size() << std::endl;
 
    std::vector<T> tmp(std::move(a));
    
    // move перемещает объект, поэтому вектор 'a' больше не содержит данных: длина 0
    std::cout << __LINE__ << ":" << a.size() << std::endl; 
 
    std::cout << __LINE__ << ":" << b.size() << std::endl; 
    a = std::move(b);
    std::cout << __LINE__ << ":" << b.size() << std::endl; 
 
    std::cout << __LINE__ << ":" << tmp.size() << std::endl;
 
    b = std::move(tmp);
 
   // move перемещает объект, поэтому вектор 'tmp' больше не содержит данных: длина 0
    std::cout << __LINE__ << ":" << tmp.size() << std::endl; 
 }

template <typename T>
void swap_rval_explicit(std::vector<T> & a, std::vector<T> & b)
{
    std::cout << __LINE__ << ":" << a.size() << std::endl;
 
    std::vector<T> tmp(static_cast<std::vector<T>&&>(a));
 
    // move перемещает объект, поэтому вектор 'a' больше не содержит данных: длина 0
    std::cout << __LINE__ << ":" << a.size() << std::endl; 
 
    std::cout << __LINE__ << ":" << b.size() << std::endl; 
    a = static_cast<std::vector<T>&&>(b);
    std::cout << __LINE__ << ":" << b.size() << std::endl; 
 
    std::cout << __LINE__ << ":" << tmp.size() << std::endl;
 
    b = static_cast<std::vector<T>&&>(tmp);
 
   // move перемещает объект, поэтому вектор 'tmp' больше не содержит данных: длина 0
    std::cout << __LINE__ << ":" << tmp.size() << std::endl; 
 }

 void swap_rval_explicit_char(std::vector<char> & a, std::vector<char> & b)
{
    std::cout << __LINE__ << ":" << a.size() << std::endl;
 
    std::vector<char> tmp(static_cast<std::vector<char>&&>(a));
 
    // rvalue перемещает объект, поэтому вектор 'a' больше не содержит данных: длина 0
    // DESTRUCTIVE READ
    std::cout << __LINE__ << ":" << a.size() << std::endl; 
 
    std::cout << __LINE__ << ":" << b.size() << std::endl; 
    a = static_cast<std::vector<char>&&>(b);
    std::cout << __LINE__ << ":" << b.size() << std::endl; 
 
    std::cout << __LINE__ << ":" << tmp.size() << std::endl;
 
    b = static_cast<std::vector<char>&&>(tmp);
 
   // rvalue перемещает объект, поэтому вектор 'tmp' больше не содержит данных: длина 0
    std::cout << __LINE__ << ":" << tmp.size() << std::endl; 
 }

 void swap_lval_explicit_char(std::vector<char> & a, std::vector<char> & b)
{
    std::cout << __LINE__ << ":" << a.size() << std::endl;
 
    std::vector<char> tmp(static_cast<std::vector<char>&>(a));
 
    // lvalue не перемещает объект, поэтому вектор 'a' по прежнему содержит данные: длина !=0
    std::cout << __LINE__ << ":" << a.size() << std::endl; 
 
    std::cout << __LINE__ << ":" << b.size() << std::endl; 
    a = static_cast<std::vector<char>&>(b);
    std::cout << __LINE__ << ":" << b.size() << std::endl; 
 
    std::cout << __LINE__ << ":" << tmp.size() << std::endl;
 
    b = static_cast<std::vector<char>&>(tmp);
 
    // lvalue не перемещает объект, поэтому вектор 'tmp' по прежнему содержит данные: длина !=0
     std::cout << __LINE__ << ":" << tmp.size() << std::endl; 
 }

 /**
   *  @brief  Convert a value to an rvalue.
   *  @param  __t  A thing of arbitrary type.
   *  @return The parameter cast to an rvalue-reference to allow moving it.
  */
template<typename _Tp>
#if 1
constexpr typename std::remove_reference<_Tp>::type&& // gcc
#else
constexpr typename std::remove_reference<_Tp&&>::type // alexeit, same as gcc
#endif 
move_gcc(_Tp&& __t) noexcept
    { return static_cast<typename std::remove_reference<_Tp>::type&&>(__t); }

int main()
{
	int i = 0;

	int &i_ref = i;
	i_ref++;
	std::cout << __LINE__ << ":" << i << std::endl;

	int &&i_ref_r{1};
	i_ref_r++;
	std::cout << __LINE__ << ":" << i_ref_r << std::endl;

	i = 3;

	int &&i_ref_rr = static_cast<int &&>(i);
	i++;
	i_ref_rr++;
	std::cout << __LINE__ << ":" << i_ref_rr << ", " << i << std::endl;

	// swap векторов с rvalue
	std::vector<char> a = {'a', 'b', 'c'};
	std::vector<char> b = {'a', 'x', 'y', 'z'};
	swap(a, b);

	swap_rval_explicit(a, b);

	swap_rval_explicit_char(a, b);

	swap_lval_explicit_char(a, b);

	// function move
	void(*pf)(std::vector<char> &, std::vector<char> &) = &swap_rval_explicit_char;

	pf(a, b);

	std::cout << __LINE__ << ":" << std::hex << (void*)pf << std::dec << std::endl;	

	void(*pf1)(std::vector<char> &, std::vector<char> &) = std::move(pf);
	std::cout << __LINE__ << ":" << std::hex << (void*)pf1 << std::dec << std::endl;	
	
#if 0
	void(*pf2)(std::vector<char> &, std::vector<char> &) = 
		static_cast<void (*)(std::vector<char> &, std::vector<char> &)&&>(pf);
#else
	void(*pf2)(std::vector<char> &, std::vector<char> &) = 
		static_cast<std::remove_reference<void (*)(std::vector<char> &, std::vector<char> &)>
		
			::type&&>(pf)
		;
	std::cout << __LINE__ << ":" << std::hex << (void*)pf2 << std::dec << std::endl;	
#endif	
	std::cout << __LINE__ << ":" << std::hex << (void*)pf << std::dec << std::endl;	


// move from gcc: https://github.com/gcc-mirror/gcc/blob/master/libstdc%2B%2B-v3/include/bits/move.h
	std::vector<char>s = {'s', 't'};
	std::vector<char>t (a);
    std::cout << __LINE__ << ":" << s.size() << std::endl;

#if 0
    std::vector<char>x (move_gcc(s)); 
#else
    move_gcc(s);
#endif   
	std::cout << __LINE__ << ":" << s.size() << std::endl;

#if 0
    std::vector<char>x (move_gcc(s)); 
#else
    std::move(s);
#endif   
	std::cout << __LINE__ << ":" << s.size() << std::endl;

// std::move (int)
	int y = 101;
	int z = std::move(y);
	std::cout << __LINE__ << ":" << y << std::endl;

	
	return 0;
}